#! /bin/bash

numBuildings=9

heightStep=10
heightMin=30
heightMax=150

orientStep=10
orientMin=0
orientMax=90

dimStep=10
xdimMin=20
xdimMax=50
ydimMin=20
ydimMax=50

# write the model_template

template=""
for i in `seq 1 $numBuildings`; do
    template=$template$(echo %b`echo $i`_height%),
done
template=${template::-1}
template=$template"|"
for i in `seq 1 $numBuildings`; do
    template=$template$(echo %b`echo $i`_orient%),
done
template=${template::-1}
template=$template"|"
for i in `seq 1 $numBuildings`; do
    template=$template$(echo %b`echo $i`_x%-%b`echo $i`_y%),
done
template=${template::-1}

echo $template > run/model_template.txt

# write the genopt input file
printf -v input "Vary{\n\n"
for i in `seq 1 $numBuildings`; do
    printf -v input "$input Parameter{Name=b"$i"_height;Min=$heightMin;Ini=$heightMin;Max=$heightMax;Step=$heightStep;}\n"
done
printf -v input "$input\n"
for i in `seq 1 $numBuildings`; do
    printf -v input "$input Parameter{Name=b"$i"_orient;Min=$orientMin;Ini=$orientMin;Max=$orientMax;Step=$heightStep;}\n"
done
printf -v input "$input\n"
for i in `seq 1 $numBuildings`; do
    printf -v input "$input Parameter{Name=b"$i"_x;Min=$xdimMin;Ini=$xdimMin;Max=$xdimMax;Step=$dimStep;}\n"
done
printf -v input "$input\n"
for i in `seq 1 $numBuildings`; do
    printf -v input "$input Parameter{Name=b"$i"_y;Min=$ydimMin;Ini=$ydimMin;Max=$ydimMax;Step=$dimStep;}\n"
done
printf -v input "$input\n}"

echo "$input" > run/input.txt

cat >> run/input.txt <<END

OptimizationSettings{
	MaxIte = 3000;
	MaxEqualResults = 100;
	WriteStepNumber = false;
	UnitsOfExecution = 30;
}

Algorithm{
  Main = PSOCC;
  NeighborhoodTopology = lbest;
  NeighborhoodSize = 30;
  NumberOfParticle = 60;
  NumberOfGeneration = 100;
  Seed = 1;
  CognitiveAcceleration = 2.8;
  SocialAcceleration = 1.3;
  MaxVelocityGainContinuous = 0.5;
  MaxVelocityDiscrete = 4;
  ConstrictionGain = 0.5;
}
END

: << "END"



# Hybrid Generalized Pattern Search Algorithm with Particle Swarm Optimization Algorithm

#Parsopoulos and Vrahatis [PV02b] use for x ∈ Rnc a population size of about 5 n up to n = 15. 
# For n ≈ 10 . . . 20, they use nP ≈ 10 n. They set the number of generations to nG = 1000 up to n = 20 and to nG = 2000 for n = 30.

# Kennedy and Eberhart [KES01] use, for test cases with the lbest neighborhood
# topology of size l = 2 and n = 2 and n = 30, a population size of
# nP = 20 . . . 30. They report that 10 . . . 50 particles usually work well. As a rule
# of thumb, they recommend for the lbest neighborhood to select the neighborhood
# size such that each neighborhood consists of 10 . . . 20% of the population.

Algorithm{
	Main = GPSPSOCCHJ;
	NeighborhoodTopology = lbest;
	NeighborhoodSize = 25;
	NumberOfParticle = 50;
	NumberOfGeneration = 50;
	Seed = 1;
	CognitiveAcceleration = 2.8;
	SocialAcceleration = 1.3;
	MaxVelocityGainContinuous = 0.5;
	MaxVelocityDiscrete = 4;
	ConstrictionGain = 0.5;
	MeshSizeDivider = 2;
	InitialMeshSizeExponent = 0;
	MeshSizeExponentIncrement = 1;
	NumberOfStepReduction = 4;
}

Algorithm{
  Main = PSOCC;
  NeighborhoodTopology = lbest;
  NeighborhoodSize = 30;
  NumberOfParticle = 60;
  NumberOfGeneration = 100;
  Seed = 1;
  CognitiveAcceleration = 2.8;
  SocialAcceleration = 1.3;
  MaxVelocityGainContinuous = 0.5;
  MaxVelocityDiscrete = 4;
  ConstrictionGain = 0.5;
}

END