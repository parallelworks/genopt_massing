#!/bin/bash

export PATH=$PATH:/core/swift-pw-bin/swift-svn/bin/

rm run/results -R

#### RUN THE OPTIMIZATION ####
echo "running the view optimization...";echo ""

# tar the input files
tar -C run/inputs -czf run/inputs.tgz .

java -classpath genopt.jar genopt.GenOpt run/massing_opt.ini
echo "optimization complete - starting post-processing..."

## create the padded image files
echo "creating padded massing images..."

#: << "ENDALL"

rm run/results/renders_plan_pad -R  >/dev/null 2>&1;mkdir run/results/renders_plan_pad
for i in run/results/renders_plan/*;do 
 num=`expr match "$(basename $i)" '[^0-9]*\([0-9]\+\).*'`
 sudo cp $i run/results/renders_plan_pad/$(printf "%04d" $num)
done

rm run/results/renders_overview_pad -R  >/dev/null 2>&1;mkdir run/results/renders_overview_pad
for i in run/results/renders_overview/*;do 
 num=`expr match "$(basename $i)" '[^0-9]*\([0-9]\+\).*'`
 sudo cp $i run/results/renders_overview_pad/$(printf "%04d" $num)
done

#### POST PROCESSING AND RESULT GENERATION ####

## create the gnuplot script for the graphrunner
numberResultFiles=$(ls run/results/areas | wc -l)
targetGFA=$(head -n 1 run/inputs/targetAreas.txt | cut -d "," -f2)
cat > run/plotIteration <<END
set terminal png size 600,400 enhanced font "Arial,8" x000000 xffffff
set output "run/plot.png"
set title "View Optimization Results"
set xlabel "Iteration"
set ylabel "View Result (%)"
set grid
set tics out
set yrange [0:100]
set xrange [0:$numberResultFiles]
plot "run/graphtmp.txt" using 1:5 title "Overall Score" with lines,"run/graphtmp.txt" using 1:8 title "Weighted View Score" with lines,"run/graphtmp.txt" using 1:7 title "GFA Error" with lines,"run/graphtmp.txt" using 1:6 title "Grass Area Error" with lines
END

## make the result graphs
echo "creating graphrunner images..."
sudo rm run/results/plots -R >/dev/null 2>&1;mkdir run/results/plots 
for i in $(seq 1 $(tail -n +21 run/OutputListingAll.txt | wc -l)); do 
 tail -n +21 run/OutputListingAll.txt | head -n $i > run/graphtmp.txt
 id=$(tail -n +21 run/OutputListingAll.txt | tail -n +$i | head -n 1 | cut -d$'\t' -f 1)
 gnuplot run/plotIteration  >/dev/null 2>&1
 mv run/plot.png run/results/plots/$(printf "%04d" $id)
done


## make the result polar thumbnails
echo "creating the polar plots..."
sudo rm run/results/polar_plots -R >/dev/null 2>&1;mkdir run/results/polar_plots 

rm run/results/areas_pad -R  >/dev/null 2>&1;mkdir run/results/areas_pad
for i in run/results/areas/*;do 
 num=`expr match "$(basename $i)" '[^0-9]*\([0-9]\+\).*'`
 sudo cp $i run/results/areas_pad/$(printf "%04d" $num)
done

for f in run/results/areas_pad/*;do 
  viewscore=$(tail -n 3 $f | head -n 1 | cut -d "," -f 2)
  diffGFA=$(tail -n 5 $f | head -n 1 | cut -d "," -f 2)
  diffGrass=$(tail -n 4 $f | head -n 1 | cut -d "," -f 2)
  sed -e "s|@@_GRASS_DIFF_@@|$diffGrass|g" -e "s|@@_GFA_DIFF_@@|$diffGFA|g" -e "s|@@_VIEW_SCORE_@@|$viewscore|g" < "run/plotPolar.template" > "run/plotPolar"
  gnuplot run/plotPolar  >/dev/null 2>&1
  mv run/polar.png run/results/polar_plots/$(basename $f)
done


# create the result txt file
echo "creating resultant file"
rm run/results/results.txt >/dev/null 2>&1;touch run/results/results.txt
for i in run/results/areas_pad/*;do 
 num=`expr match "$(basename $i)" '[^0-9]*\([0-9]\+\).*'`
 echo $num >> run/results/results.txt
done

## convert resulting images to gif
echo "converting resulting images to a gif animation...";
convert -delay 3 -loop 1 -gravity South -background Black -fill White -splice 0x18 -annotate +0+2 "%f" run/results/renders_overview_pad/* overview.gif
convert -delay 3 -loop 1 -gravity South -background Black -fill White -splice 0x18 -annotate +0+2 "%f" run/results/renders_plan_pad/* plan.gif
convert -delay 3 -loop 1 run/results/plots/* plot.gif
convert -delay 3 -loop 1 -gravity South -background Black -fill White -splice 0x18 -annotate +0+2 "%f" run/results/polar_plots/* polar.gif

#ENDALL
