#! /bin/bash

export GLOBUS_HOSTNAME=52.27.116.108
export GLOBUS_TCP_PORT_RANGE=3000,3050
export GLOBUS_TCP_SOURCE_RANGE=3000,3050

coaster-service -p 3015 -localport 3016 -stats -nosec -passive
