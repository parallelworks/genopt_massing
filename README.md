GenOpt Urban Planning Optimization
====


Dependencies

```
sudo apt-get install build-essential git wget bc -y
sudo apt-get install default-jre -y
sudo apt-get install libvtk5-dev python-vtk -y
sudo apt-get install xvfb -y
sudo apt-get install imagemagick -y
sudo apt-get install python-dev -y
sudo apt-get install python-pip -y
pip install numpy
pip install matplotlib
pip install ipython
wget -qO- https://get.docker.com/ | sh
docker pull mattshax/cadquery
```

# must launch the workflow with priliedged flag
