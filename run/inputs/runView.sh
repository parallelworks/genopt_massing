#! /bin/bash

mkdir output

echo "./genSite.sh $(cat $1)"
./genSite.sh $(cat $1)

# get GFA target and difference from target and append to areas.txt
totalGFA=$(head -n 1 output/areas.txt | cut -d "," -f2)
totalGrassPercent=$(head -n 3 output/areas.txt | tail -n 1 | cut -d "," -f2)

targetGFA=$(head -n 1 ./targetAreas.txt | cut -d "," -f2)
targetGrassPercent=$(head -n 2 ./targetAreas.txt | tail -n 1 | cut -d "," -f2)

calc () {
    bc -l <<< "$@"
}
round()
{
    echo $(printf %.$2f $(echo "scale=$2;(((10^$2)*$1)+0.5)/(10^$2)" | bc))
}

diffArea=$(round $(calc $(calc $(calc $totalGFA-$targetGFA)/$targetGFA )*100) 3)
diffGrassPercent=$(round $(calc $(calc $(calc $totalGrassPercent-$targetGrassPercent)/$targetGrassPercent )*100) 3)

echo -e "\ntargetGFA,$targetGFA" >> $(dirname $1)/output/areas.txt
echo diffGFAPercent,$diffArea | tr -d - >> $(dirname $1)/output/areas.txt
echo diffGrassPercent,$diffGrassPercent | tr -d - >> $(dirname $1)/output/areas.txt

# only take a plan snapshot
./genViewscore.sh plan.pvcc false plan

# render overview and run view study
./genViewscore.sh overview_north.pvcc true overview
