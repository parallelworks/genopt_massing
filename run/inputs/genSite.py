
from cadquery import *
import cadquery
import math
import sys
import os
import copy

from OCC.TopoDS import TopoDS_Shape
from OCC.StlAPI import StlAPI_Reader
from OCC.GProp import GProp_GProps
from OCC.BRepGProp import brepgprop_VolumeProperties

# units in meters
# params = "100,50,50,25,25,50,50,50,100|55,0,-75,0,30,0,0,45,0|50_50,30_30,30_30,30_30,50_30,30_30,30_30,30_30,30_30"
params = sys.argv[1].split("|")
outdir = sys.argv[2]

# these are the parametric building height and dimension variables
buildingHeights = [float(i) for i in params[0].split(",")]
buildingOrient = [float(i) for i in params[1].split(",")]
buildingDims = params[2].split(",")

# static site variables
SiteWidth = 300
SiteLength = 300
BlockWidth = 100
BlockLength = 100
StreetWidth = 12
SidewalkWidth = 3
BlockHeight = 0.25
StreetHeight = 0.1
FloorHeight = 5
ModuleSize = 5

# HELPER CLASSES

class Quaternion():
    def __init__(self, x=0.0, y=0.0, z=0.0, w=1.0):
        self.x = x
        self.y = y
        self.z = z
        self.w = w
        
    def setFromAxisAngle ( self, axis, angle ):
		# http://www.euclideanspace.com/maths/geometry/rotations/conversions/angleToQuaternion/index.htm
		halfAngle = angle / 2 
		s = math.sin( halfAngle )
		self.x = axis.x * s
		self.y = axis.y * s
		self.z = axis.z * s
		self.w = math.cos( halfAngle )
		return self
		
class Vector():
    def __init__(self, x=0.0, y=0.0, z=0.0):
        self.x = x
        self.y = y
        self.z = z

    def __sub__(self, other):
        self.x -= other.x
        self.y -= other.y
        self.z -= other.z
        return self

    def dist(self, other):
        return math.sqrt((self.x-other.x)**2 + (self.y-other.y)**2 + (self.z-other.z)**2)

    def unitVector(self):
        mag = self.dist(Vector())
        if mag != 0.0:
            return Vector(self.x * 1.0/mag, self.y * 1.0/mag, self.z * 1.0/mag)
        else:
            return Vector()
    
    def applyAxisAngle(self,axis,angle):
        q = Quaternion(0,0,0,1)
        self.applyQuaternion( q.setFromAxisAngle( axis, angle ) )
        return self
		
    def applyQuaternion(self,q):
		x = self.x;
		y = self.y;
		z = self.z;
		qx = q.x;
		qy = q.y;
		qz = q.z;
		qw = q.w;
		ix =  qw * x + qy * z - qz * y;
		iy =  qw * y + qz * x - qx * z;
		iz =  qw * z + qx * y - qy * x;
		iw = - qx * x - qy * y - qz * z;
		self.x = ix * qw + iw * - qx + iy * - qz - iz * - qy;
		self.y = iy * qw + iw * - qy + iz * - qx - ix * - qz;
		self.z = iz * qw + iw * - qz + ix * - qy - iy * - qx;
		return self


# SITE FUNCTIONS

def genSite(height):
    # can pass any polygon here, but starting with simple rect
    pts = [
        (0, 0),
        (0, SiteLength),
        (SiteWidth, SiteLength),
        (SiteWidth, 0),
        (0, 0)
    ]
    return cadquery.Workplane("XY").polyline(pts).extrude(height)

def genBlocks(k):
    block = genSite(k)
    # draw parametric streets
    residual = SiteLength - int(math.floor(SiteLength/BlockLength))*BlockLength
    for x in range(0, int(math.floor(SiteLength/BlockLength))+1):
        roadpts = [
            (0,+StreetWidth/2 + x*BlockLength +residual/2),
            (0,-StreetWidth/2 + x*BlockLength+residual/2),
            (SiteWidth,-StreetWidth/2 + x*BlockLength+residual/2),
            (SiteWidth,+StreetWidth/2 + x*BlockLength+residual/2),
            (0,+StreetWidth/2 + x*BlockLength+residual/2)
            ]
        line = cadquery.Workplane("XY").polyline(roadpts).extrude(10) 
        block.cut(line)
    residual = SiteWidth - int(math.floor(SiteWidth/BlockWidth))*BlockWidth
    for x in range(0, int(math.floor(SiteWidth/BlockWidth))+1):
        roadpts = [
            (+StreetWidth/2 + x*BlockWidth +residual/2,0),
            (-StreetWidth/2 + x*BlockWidth+residual/2,0),
            (-StreetWidth/2 + x*BlockWidth+residual/2,SiteLength),
            (+StreetWidth/2 + x*BlockWidth+residual/2,SiteLength),
            (+StreetWidth/2 + x*BlockWidth+residual/2,0)
            ]
        line = cadquery.Workplane("XY").polyline(roadpts).extrude(10)                                              
        block.cut(line)
    return block

def genLandscape(k):
    block = genSite(k)
    # draw parametric streets
    residual = SiteLength - int(math.floor(SiteLength/BlockLength))*BlockLength
    for x in range(0, int(math.floor(SiteLength/BlockLength))+1):
        roadpts = [
            (0,SidewalkWidth+StreetWidth/2 + x*BlockLength +residual/2),
            (0,-SidewalkWidth-StreetWidth/2 + x*BlockLength+residual/2),
            (SiteWidth,-SidewalkWidth-StreetWidth/2 + x*BlockLength+residual/2),
            (SiteWidth,SidewalkWidth+StreetWidth/2 + x*BlockLength+residual/2),
            (0,SidewalkWidth+StreetWidth/2 + x*BlockLength+residual/2)
            ]
        line = cadquery.Workplane("XY").polyline(roadpts).extrude(1) 
        block.cut(line)
    residual = SiteWidth - int(math.floor(SiteWidth/BlockWidth))*BlockWidth
    for x in range(0, int(math.floor(SiteWidth/BlockWidth))+1):
        roadpts = [
            (SidewalkWidth+StreetWidth/2 + x*BlockWidth +residual/2,0),
            (-SidewalkWidth-StreetWidth/2 + x*BlockWidth+residual/2,0),
            (-SidewalkWidth-StreetWidth/2 + x*BlockWidth+residual/2,SiteLength),
            (SidewalkWidth+StreetWidth/2 + x*BlockWidth+residual/2,SiteLength),
            (SidewalkWidth+StreetWidth/2 + x*BlockWidth+residual/2,0)
            ]
        line = cadquery.Workplane("XY").polyline(roadpts).extrude(1)                                              
        block.cut(line)
    return block

blocks = genBlocks(BlockHeight)
landscape = genLandscape(BlockHeight) #.translate((0,0,BlockHeight))
roads = genSite(StreetHeight).cut(blocks)

# BUILDING FUNCTIONS

# get the block centerpoints
buildingCenters=[]
def pushBuildingPoints(i):
    if (i.z==BlockHeight):
        buildingCenters.append( (i.x,i.y,i.z) )
blocks.faces().eachpoint(pushBuildingPoints)

# sort the buildingcenters by distance from 0,0
buildingCenters = sorted(buildingCenters, key=lambda tup: (tup[0],tup[1]))

def getOriginPoints(e):
    for i in range(0,len(e.Vertices())):
        if (i == len(e.Vertices()) - 1) :
            start= i
            end= 0
        else:
            start = i
            end = i+1
        v1=Vector(e.Vertices()[start].X,e.Vertices()[start].Y,0)
        v2=Vector(e.Vertices()[end].X,e.Vertices()[end].Y,0)
        p0 = v1;
        p1 = v2;
        dist = v2.dist(v1);
        dir=copy.copy(v2)
        dir1=copy.copy(v1)
        dir.__sub__(dir1)
        normal = dir.applyAxisAngle(Vector(0,0,-1),math.pi*90/180)
        t0 = 0;
        t1 = (math.floor(dist / ModuleSize))-1.0;
        for o in range(1,int(math.floor(dist / ModuleSize))) :
            t = o;
            dt = (t - t0) / (t1 - t0 );
            pt=Vector(0,0,0)
            pt.x = p0.x + (dt * (p1.x - p0.x));
            pt.y = p0.y + (dt * (p1.y - p0.y));
            pt.z = FloorHeight/2+BlockHeight;
            basepts.append(pt)
            normpts.append(normal)

buildings=[]
originpts=[]
totalGFA=0

for x in range(0,len(buildingCenters)):
#for x in range(4,5):
   totalFloors=math.floor(buildingHeights[x]/FloorHeight)
   totalGFA+=float(buildingDims[x].split("-")[0])*float(buildingDims[x].split("-")[1])*totalFloors

   rect = cadquery.Workplane("XY").center(buildingCenters[x][0],buildingCenters[x][1])\
   .rect(float(buildingDims[x].split("-")[0]),float(buildingDims[x].split("-")[1])).rotateAboutCenter((0,0, 1000000000000000000),buildingOrient[x])
   basepts=[]
   normpts=[]
   rect.each(getOriginPoints)

   #replicate up the building
   for o in range(0,int(math.ceil(buildingHeights[x] / FloorHeight))) :
       for k in range(0,len(basepts)) :
           # get the normal point extend from the surface
           ptNormal = copy.copy(basepts[k])
           normal = normpts[k]
           length = 0.05;
           ptNormal.x = ptNormal.x + normal.x * length;
           ptNormal.y = ptNormal.y + normal.y * length;
           originpts.append(str(basepts[k].x)+","+str(basepts[k].y)+","+str(basepts[k].z + (o * FloorHeight)) +"|"+ str(ptNormal.x)+","+str(ptNormal.y)+","+str(basepts[k].z + (o * FloorHeight)))
           
   build = cadquery.Workplane("XY").center(buildingCenters[x][0],buildingCenters[x][1])\
   .box(float(buildingDims[x].split("-")[0]),float(buildingDims[x].split("-")[1]),buildingHeights[x])\
   .translate((0,0,buildingHeights[x]/2.0)).rotateAboutCenter((0,0, 1000000000000000000),buildingOrient[x])
   buildings.append(build)

# merge all pins to a single object
merged_buildings = buildings[0]
for b in buildings[1:]:
    merged_buildings = merged_buildings.union(b)

# final merging
buildings=merged_buildings
mb=merged_buildings
tmp = landscape
sidewalks=blocks.cut(tmp)
grass = landscape.cut(mb)

# EXPORT FUNCTIONS
roadPercent=0
grassPercent=0
sidewalkPercent=0

def getArea(file):
    stl_reader = StlAPI_Reader()
    shape = TopoDS_Shape()
    stl_reader.Read(shape,file)
    props = GProp_GProps()
    brepgprop_VolumeProperties(shape, props)
    name=os.path.basename(file).split(".stl")[0]
    print name+" "+str(int(abs(props.Mass()/BlockHeight)))+" square meters"
    with open(outdir+'areas.txt', 'ab') as f:
        f.write("\n"+name+","+str(int(abs(props.Mass()/BlockHeight))))
    f.closed
    # get percentages on site
    if name=="grass":
        global grassPercent
        grassPercent=float(abs(props.Mass()/BlockHeight))/(SiteLength*SiteWidth)*100
        with open(outdir+'areas.txt', 'ab') as f:
            f.write("\ngrassPercent,"+str( float(abs(props.Mass()/BlockHeight)) / (SiteLength*SiteWidth)*100) )
        f.closed
    elif name=="roads":
        global roadPercent
        roadPercent=float(abs(props.Mass()/BlockHeight))/(SiteLength*SiteWidth)*100
        with open(outdir+'areas.txt', 'ab') as f:
            f.write("\nroadPercent,"+str( float(abs(props.Mass()/BlockHeight)) / (SiteLength*SiteWidth)*100) )
        f.closed
    elif name=="sidewalks":
        global sidewalkPercent
        sidewalkPercent=float(abs(props.Mass()/BlockHeight))/(SiteLength*SiteWidth)*100
        with open(outdir+'areas.txt', 'ab') as f:
            f.write("\nsidewalkPercent,"+str( float(abs(props.Mass()/BlockHeight)) / (SiteLength*SiteWidth)*100) )
        f.closed

def exportGeometry():
    with open(outdir+'buildings.stl', 'w') as f:
        f.write(exporters.toString(buildings, 'STL'))
    f.closed
    with open(outdir+'sidewalks.stl', 'w') as f:
        f.write(exporters.toString(sidewalks, 'STL'))
    f.closed
    with open(outdir+'grass.stl', 'w') as f:
        f.write(exporters.toString(grass, 'STL'))
    f.closed
    with open(outdir+'roads.stl', 'w') as f:
        f.write(exporters.toString(roads, 'STL'))
    f.closed
    
    print "gfa: "+str(int(totalGFA))+" square meters"
    with open(outdir+'areas.txt', 'wb') as f:
        f.write("totalgfa,"+str(int(totalGFA)))
    f.closed
    
    getArea(outdir+"grass.stl")
    getArea(outdir+"roads.stl")
    getArea(outdir+"sidewalks.stl")

    with open(outdir+'areas.txt', 'ab') as f:
            f.write("\nfootprintPercent,"+str( 100-(roadPercent+sidewalkPercent+grassPercent)) )
    f.closed

    with open(outdir+'originpts.txt', 'wb') as f:
            f.write( "\n".join(originpts) )
    f.closed

exportGeometry()
