
# coding: utf-8

# ## Imports

# In[313]:

import vtk
import numpy
import matplotlib.pyplot as plt
import urllib2
import sys
import math
import xml.etree.ElementTree as ET


path = sys.argv[1]
camera_file = sys.argv[2]
run_view = sys.argv[3]
render_name = sys.argv[4]

# ---

# ## Helper-functions

# We're gonna use this function to embed a still image of a VTK render

# In[314]:

import vtk
from IPython.display import Image

def vtk_show(renderer, width=400, height=300, imagename="tmp.png"):
    """
    Takes vtkRenderer instance and returns an IPython Image with the rendering.
    """
    renderWindow = vtk.vtkRenderWindow()
    renderWindow.SetOffScreenRendering(1)
    renderWindow.AddRenderer(renderer)
    renderWindow.SetSize(width, height)
    renderWindow.Render()
     
    windowToImageFilter = vtk.vtkWindowToImageFilter()
    windowToImageFilter.SetInput(renderWindow)
    windowToImageFilter.Update()

    writer = vtk.vtkPNGWriter()
    writer.SetFileName(imagename+".png")
    writer.SetInput(windowToImageFilter.GetOutput())
    writer.Write()
    
    writer = vtk.vtkPNGWriter()
    writer.SetWriteToMemory(1)
    writer.SetInputConnection(windowToImageFilter.GetOutputPort())
    writer.Write()
    data = str(buffer(writer.GetResult()))
    
    return Image(data)


# This function adds a line to a `vtkRenderer`

# In[315]:

def addLine(renderer, p1, p2, color=[0.0, 0.0, 1.0], opacity=1.0):
    line = vtk.vtkLineSource()
    line.SetPoint1(p1)
    line.SetPoint2(p2)

    mapper = vtk.vtkPolyDataMapper()
    mapper.SetInputConnection(line.GetOutputPort())

    actor = vtk.vtkActor()
    actor.SetMapper(mapper)
    actor.GetProperty().SetColor(color)
    actor.GetProperty().SetOpacity(opacity)
    actor.GetProperty()

    renderer.AddActor(actor)


# This function adds a point (a sphere really) to a `vtkRenderer`

# In[316]:

def addPoint(renderer, p, color=[0.0, 0.0, 0.0], radius=1, resolution=8):
    point = vtk.vtkSphereSource()
    point.SetCenter(p)
    point.SetRadius(radius)
    point.SetPhiResolution(resolution)
    point.SetThetaResolution(resolution)

    mapper = vtk.vtkPolyDataMapper()
    mapper.SetInputConnection(point.GetOutputPort())

    actor = vtk.vtkActor()
    actor.SetMapper(mapper)
    actor.GetProperty().SetColor(color)

    renderer.AddActor(actor)


# Quick conversion between a `numpy.ndarray` and a `list`

# In[317]:

l2n = lambda l: numpy.array(l)
n2l = lambda n: list(n)


# Helper function to load STL file

# In[318]:

def loadSTL(filenameSTL):
    readerSTL = vtk.vtkSTLReader()
    readerSTL.SetFileName(filenameSTL)
    # 'update' the reader i.e. read the .stl file
    readerSTL.Update()

    polydata = readerSTL.GetOutput()

    # If there are no points in 'vtkPolyData' something went wrong
    if polydata.GetNumberOfPoints() == 0:
        raise ValueError(
            "No point data could be loaded from '" + filenameSTL)
        return None
    
    return polydata


# Helper function for timer functionality

# In[330]:

import time

def TicTocGenerator():
    # Generator that returns time differences
    ti = 0           # initial time
    tf = time.time() # final time
    while True:
        ti = tf
        tf = time.time()
        yield tf-ti # returns the time difference

TicToc = TicTocGenerator() # create an instance of the TicTocGen generator

# This will be the main function through which we define both tic() and toc()
def toc(tempBool=True):
    # Prints the time difference yielded by generator instance TicToc
    tempTimeInterval = next(TicToc)
    if tempBool:
        with open(path+'areas.txt', 'ab') as f:
            f.write( "\ntime,%f" %tempTimeInterval)
        f.closed

def tic():
    # Records a time in TicToc, marks the beginning of a time interval
    toc(False)


# ---

# ## Options

# Environment options

# In[394]:

# RAY OPTIONS
# Length of rays cast from the sun. Since the rays we
# cast are finite lines we set a length appropriate to the scene.
# CAUTION: If your rays are too short they won't hit the earth 
# and ray-tracing won't be possible. Its better for the rays to be
# longer than necessary than the other way around
RayCastLength = 1000.0

# COLOR OPTIONS
ColorBackground = [0, 0, 0]
ColorLight = [1.0, 1.0, 0.0]
ColorBasePoints = [0,0,1]
ColorBaseNormals = [1,1,0]
ColorTargetPoints = [1,0,0]
ColorResultPoints = [0, 1.0, 0]
ColorRayHit = [1, 0, 0]
ColorRayMiss = [0, 1.0, 0]
OpacityRayMiss = 0.5

ColorBuildings = [1, 1, 1]
ColorGrass = [0.1, 0.9, 0.1]
ColorRoads = [0.5, 0.5, 0.5]
ColorSidewalk = [1, 1, 1]


# ---

# ## STL Loading

# In[418]:

buildings = loadSTL(path+"buildings.stl")
grass = loadSTL(path+"grass.stl")
road = loadSTL(path+"roads.stl")
sidewalk = loadSTL(path+"sidewalks.stl")

#building properties
mbuild = vtk.vtkPolyDataMapper()
mbuild.SetInput(buildings)
abuild = vtk.vtkActor()
abuild.SetMapper(mbuild)
abuild.GetProperty().SetOpacity(1)
abuild.GetProperty().SetColor(ColorBuildings)

#grass properties
mgrass = vtk.vtkPolyDataMapper()
mgrass.SetInput(grass)
agrass = vtk.vtkActor()
agrass.SetMapper(mgrass)
agrass.GetProperty().SetOpacity(0.5)
agrass.GetProperty().SetColor(ColorGrass)

#road properties
mroad = vtk.vtkPolyDataMapper()
mroad.SetInput(road)
aroad = vtk.vtkActor()
aroad.SetMapper(mroad)
aroad.GetProperty().SetOpacity(0.35)
aroad.GetProperty().SetColor(ColorRoads)

#sidewalk properties
msidewalk = vtk.vtkPolyDataMapper()
msidewalk.SetInput(sidewalk)
asidewalk = vtk.vtkActor()
asidewalk.SetMapper(msidewalk)
asidewalk.GetProperty().SetOpacity(0.5)
asidewalk.GetProperty().SetColor(ColorSidewalk)

def getRenderer():
    renderer = vtk.vtkRenderer()
    
    renderer.AddActor(abuild)
    renderer.AddActor(agrass)
    renderer.AddActor(aroad)
    renderer.AddActor(asidewalk)
    
    renderer.SetBackground(ColorBackground)

    camera = renderer.MakeCamera()

    light = vtk.vtkLight()
    light.SetAmbientColor(ColorLight)
    light.SetConeAngle(180)

    tree = ET.parse(camera_file)
    root = tree.getroot()
    for Proxy in root:
        for Property in Proxy:
            if (Property.get('name')=="CameraPosition"):
                camera.SetPosition(float(Property[0].get('value')),float(Property[1].get('value')),float(Property[2].get('value')))
                light.SetPosition(float(Property[0].get('value')),float(Property[1].get('value')),float(Property[2].get('value')))
            if (Property.get('name')=="CameraFocalPoint"):
                camera.SetFocalPoint(float(Property[0].get('value')),float(Property[1].get('value')),float(Property[2].get('value')))
                light.SetFocalPoint(float(Property[0].get('value')),float(Property[1].get('value')),float(Property[2].get('value')))
            if (Property.get('name')=="CameraViewUp"):
                camera.SetViewUp(float(Property[0].get('value')),float(Property[1].get('value')),float(Property[2].get('value')))
            if (Property.get('name')=="CameraViewAngle"):
                camera.SetViewAngle(float(Property[0].get('value')))
            if (Property.get('name')=="CameraParallelScale"):
                camera.SetParallelScale(float(Property[0].get('value')))
            if (Property.get('name')=="CameraParallelProjection"):
                camera.SetParallelProjection(float(Property[0].get('value')))

    camera.SetClippingRange(0.1, 1000000)
    renderer.SetActiveCamera(camera)

    light.SetPositional(True)
    renderer.AddLight(light)
    
    return renderer

renderer=getRenderer()



#vtk_show(renderer, 600, 400, path+render_name)

# ##Target Points for Directional Weighting

# weightscore scale between 0-viewscoreMax
# only optimize for north facing views now
weightNorth=1
weightEast=0
weightSouth=0
weightWest=0

NorthPts=[]
EastPts=[]
SouthPts=[]
WestPts=[]

def getDirectionalTargetPoints():
    
    targetPoints=[]
    weightArray=[]
    
    n=20
    nh=10
    heightSpace=15
    dirScale=250
    dx=150
    dy=150
    
    # point colors for testing
    northPts=[1, 1, 0]
    southPts=[1, 0, 0]
    eastPts=[0, 1, 0]
    westPts=[0, 1, 1]
    
    for h in range(nh):
        for i in range(n):
            v = math.cos(i * 2 * math.pi / n), math.sin(i * 2 * math.pi / n);
            point=[v[0]*dirScale+dx,v[1]*dirScale+dy,heightSpace*h]
            targetPoints.append(point)
            
            # pick out the n / s / e / w points for weighting
            angleDeg=math.atan2(v[1], v[0]) * 180 / math.pi;

            #EAST
            if angleDeg>=-45 and angleDeg<45:
                weightArray.append(weightEast)
                #addPoint(renderer, point, eastPts,weightEast,8)
            #NORTH
            elif angleDeg>=45 and angleDeg<135:
                weightArray.append(weightNorth)
                #addPoint(renderer, point, northPts,weightNorth,8)
            #SOUTH
            elif angleDeg>=-135 and angleDeg<-45:
                weightArray.append(weightSouth)
                #addPoint(renderer, point, southPts,weightSouth,8)
            #WEST
            else:
                weightArray.append(weightWest)
                #addPoint(renderer, point, westPts,weightWest,8)
                
    return targetPoints,weightArray
    
targetPoints,weightArray = getDirectionalTargetPoints()

#vtk_show(renderer, 600, 400, path+"render")

# ---

# ## Base Points and Base Normal Loading Function

# In[413]:

# read text file of base points and normals from URL

#renderer = getRenderer()

def getBasePoints():
    basePoints=[]
    text_file = open(path+"originpts.txt", "r")
    lines = text_file.read().split('\n')
    for line in lines:
        start=line.split("|")[0]
        startpoint=[float(start.split(",")[0]),float(start.split(",")[1]),float(start.split(",")[2])]
        #addPoint(renderer, startpoint, [1, 0, 0],1,8)
        end=line.split("|")[1]
        endpoint=[float(end.split(",")[0]),float(end.split(",")[1]),float(end.split(",")[2])]
        #addPoint(renderer, endpoint, [1, 1, 0],1,8)
        #addLine(renderer, startpoint, endpoint,ColorBaseNormals)
        basePoints.append(endpoint);
    return basePoints

basePoints = getBasePoints()

    
if (run_view == "false"):
        
    vtk_show(renderer, 600, 400, path+render_name)

else:
    
    # ## Prepare for Ray-Tracing
    
    # Create `vtkOBBTree` to test intersections with `buildings`
    
    # In[416]:
    
    obbBuildings = vtk.vtkOBBTree()
    obbBuildings.SetDataSet(buildings)
    obbBuildings.BuildLocator()
    
    
    # Define intersection helper-functions
    
    # In[415]:
    
    def isHit(obbTree, pSource, pTarget):
        code = obbTree.IntersectWithLine(pSource, pTarget, None, None)
        if code==0:
            return False
        return True
    
    def GetIntersect(obbTree, pSource, pTarget):
        
        # Create an empty 'vtkPoints' object to store the intersection point coordinates
        points = vtk.vtkPoints()
        # Create an empty 'vtkIdList' object to store the ids of the cells that intersect
        # with the cast rays
        cellIds = vtk.vtkIdList()
        
        # Perform intersection
        code = obbTree.IntersectWithLine(pSource, pTarget, points, cellIds)
        
        # Get point-data 
        pointData = points.GetData()
        # Get number of intersection points found
        noPoints = pointData.GetNumberOfTuples()
        # Get number of intersected cell ids
        noIds = cellIds.GetNumberOfIds()
        
        assert (noPoints == noIds)
        
        # Loop through the found points and cells and store
        # them in lists
        pointsInter = []
        cellIdsInter = []
        for idx in range(noPoints):
            pointsInter.append(pointData.GetTuple3(idx))
            cellIdsInter.append(cellIds.GetId(idx))
        
        return pointsInter, cellIdsInter
    
    
    # ## Perform ray-casting operations and visualize different aspects
    
    # In[417]:
    
    baseViews=[]
    
    tic()
    
    weightScore=0
    
    for basePoint in basePoints:
        targetViews=[]
        #for targetPoint in targetPoints:
        #for tp in xrange(0,10):
        for tp in xrange(0,len(targetPoints)):
            targetPoint=targetPoints[tp]
            # need to check if above distance threshold
            # need to check if within a 60degree horz/vert viewcone of normal
            if (isHit(obbBuildings, basePoint, targetPoint)==False):
                    targetViews.append(1)
                    weightScore=weightScore+weightArray[tp]
                    #addLine(renderer, basePoint, targetPoint, ColorRayMiss, 0.01)
            else:
                    targetViews.append(0)
                    #pointsInter, cellIdsInter = GetIntersect(obbBuildings, basePoint, targetPoint)
                    #addLine(renderer, basePoint, targetPoint, ColorRayHit, 0.01)
                    #addPoint(renderer, pointsInter[0], ColorRayHit,1)
        baseViews.append(sum(targetViews))
    
    minBaseViews=min(baseViews)
    maxBaseViews=max(baseViews)
    
    weightScore=round(float(weightScore)/(len(basePoints)*((len(targetPoints)/4*weightNorth)+(len(targetPoints)/4*weightEast)+(len(targetPoints)/4*weightSouth)+(len(targetPoints)/4*weightWest)))*100,2)
    totalScore=round(float(sum(baseViews))/(len(basePoints)*len(targetPoints))*100,2)
    
    print weightScore
    
    with open(path+'areas.txt', 'ab') as f:
                f.write( "weightedviewscore,"+str(weightScore) )
    f.closed
    
    with open(path+'areas.txt', 'ab') as f:
                f.write( "\nweightedcollidescore,"+str(100-weightScore) )
    f.closed
    
    # print "viewscore,"+str(totalScore)
    # print "collidescore,"+str(100-totalScore)
    
    # render the resulting view scores as dots
    #maxSphereSize=4
    #for pt in xrange(len(startPoints)):
    #    baseSize=float(baseViews[pt]-minBaseViews)/(maxBaseViews-minBaseViews)
    #    addPoint(renderer, startPoints[pt], ColorResultPoints,baseSize*maxSphereSize,1)
    
    toc()
    
    vtk_show(renderer, 400, 400, path+render_name)
    
