#!/bin/bash

vars=$1
echo "Generating Parametric Site for "$vars
sudo docker run -v $PWD:$PWD -w $PWD mattshax/cadquery python genSite.py $vars
echo "Site Generated"
