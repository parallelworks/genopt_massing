
from cadquery import *
import cadquery
import math
import sys

from OCC.TopoDS import TopoDS_Shape
from OCC.StlAPI import StlAPI_Reader
from OCC.GProp import GProp_GProps
from OCC.BRepGProp import brepgprop_VolumeProperties

# units in meters
# params = "100,50,50,25,25,50,50,50,100|55,0,-75,0,30,0,0,45,0|50_50,30_30,30_30,30_30,50_30,30_30,30_30,30_30,30_30"
params = sys.argv[1].split("|")

# these are the parametric building height and dimension variables
buildingHeights = [float(i) for i in params[0].split(",")]
buildingOrient = [float(i) for i in params[1].split(",")]
buildingDims = params[2].split(",")

# static site variables
SiteWidth = 300
SiteLength = 300
BlockWidth = 100
BlockLength = 100
StreetWidth = 12
SidewalkWidth = 3
BlockHeight = 0.25
StreetHeight = 0.1
FloorHeight = 5

# SITE FUNCTIONS

def genSite(height):
    # can pass any polygon here, but starting with simple rect
    pts = [
        (0, 0),
        (0, SiteLength),
        (SiteWidth, SiteLength),
        (SiteWidth, 0),
        (0, 0)
    ]
    return cadquery.Workplane("XY").polyline(pts).extrude(height)

def genBlocks(k):
    block = genSite(k)
    # draw parametric streets
    residual = SiteLength - int(math.floor(SiteLength/BlockLength))*BlockLength
    for x in range(0, int(math.floor(SiteLength/BlockLength))+1):
        roadpts = [
            (0,+StreetWidth/2 + x*BlockLength +residual/2),
            (0,-StreetWidth/2 + x*BlockLength+residual/2),
            (SiteWidth,-StreetWidth/2 + x*BlockLength+residual/2),
            (SiteWidth,+StreetWidth/2 + x*BlockLength+residual/2),
            (0,+StreetWidth/2 + x*BlockLength+residual/2)
            ]
        line = cadquery.Workplane("XY").polyline(roadpts).extrude(10) 
        block.cut(line)
    residual = SiteWidth - int(math.floor(SiteWidth/BlockWidth))*BlockWidth
    for x in range(0, int(math.floor(SiteWidth/BlockWidth))+1):
        roadpts = [
            (+StreetWidth/2 + x*BlockWidth +residual/2,0),
            (-StreetWidth/2 + x*BlockWidth+residual/2,0),
            (-StreetWidth/2 + x*BlockWidth+residual/2,SiteLength),
            (+StreetWidth/2 + x*BlockWidth+residual/2,SiteLength),
            (+StreetWidth/2 + x*BlockWidth+residual/2,0)
            ]
        line = cadquery.Workplane("XY").polyline(roadpts).extrude(10)                                              
        block.cut(line)
    return block

def genLandscape(k):
    block = genSite(k)
    # draw parametric streets
    residual = SiteLength - int(math.floor(SiteLength/BlockLength))*BlockLength
    for x in range(0, int(math.floor(SiteLength/BlockLength))+1):
        roadpts = [
            (0,SidewalkWidth+StreetWidth/2 + x*BlockLength +residual/2),
            (0,-SidewalkWidth-StreetWidth/2 + x*BlockLength+residual/2),
            (SiteWidth,-SidewalkWidth-StreetWidth/2 + x*BlockLength+residual/2),
            (SiteWidth,SidewalkWidth+StreetWidth/2 + x*BlockLength+residual/2),
            (0,SidewalkWidth+StreetWidth/2 + x*BlockLength+residual/2)
            ]
        line = cadquery.Workplane("XY").polyline(roadpts).extrude(1) 
        block.cut(line)
    residual = SiteWidth - int(math.floor(SiteWidth/BlockWidth))*BlockWidth
    for x in range(0, int(math.floor(SiteWidth/BlockWidth))+1):
        roadpts = [
            (SidewalkWidth+StreetWidth/2 + x*BlockWidth +residual/2,0),
            (-SidewalkWidth-StreetWidth/2 + x*BlockWidth+residual/2,0),
            (-SidewalkWidth-StreetWidth/2 + x*BlockWidth+residual/2,SiteLength),
            (SidewalkWidth+StreetWidth/2 + x*BlockWidth+residual/2,SiteLength),
            (SidewalkWidth+StreetWidth/2 + x*BlockWidth+residual/2,0)
            ]
        line = cadquery.Workplane("XY").polyline(roadpts).extrude(1)                                              
        block.cut(line)
    return block


blocks = genBlocks(BlockHeight)
landscape = genLandscape(BlockHeight) #.translate((0,0,BlockHeight))
roads = genSite(StreetHeight).cut(blocks)

# BUILDING FUNCTIONS

# get the block centerpoints
buildingCenters=[]
def pushBuildingPoints(i):
    if (i.z==BlockHeight):
        buildingCenters.append( (i.x,i.y,i.z) )
blocks.faces().eachpoint(pushBuildingPoints)

# sort the buildingcenters by distance from 0,0
buildingCenters = sorted(buildingCenters, key=lambda tup: (tup[0],tup[1]))

buildings=[]
buildingTemplates=[]
totalGFA=0
for x in range(0,len(buildingCenters)):
   # get the total gfa of the buildings
   totalFloors=math.floor(buildingHeights[x]/FloorHeight)
   totalGFA+=float(buildingDims[x].split("-")[0])*float(buildingDims[x].split("-")[1])*totalFloors
   build = cadquery.Workplane("XY").center(buildingCenters[x][0],buildingCenters[x][1])\
   .box(float(buildingDims[x].split("-")[0]),float(buildingDims[x].split("-")[1]),buildingHeights[x])\
   .translate((0,0,buildingHeights[x]/2.0)).rotateAboutCenter((0,0, 1000000000000000000),buildingOrient[x])
   buildings.append(build)

# merge all pins to a single object
merged_buildings = buildings[0]
for b in buildings[1:]:
    merged_buildings = merged_buildings.union(b)

# final merging
buildings=merged_buildings
mb=merged_buildings
tmp = landscape
sidewalks=blocks.cut(tmp)
grass = landscape.cut(mb)

# EXPORT FUNCTIONS

def getArea(file):
    stl_reader = StlAPI_Reader()
    shape = TopoDS_Shape()
    stl_reader.Read(shape,file)
    props = GProp_GProps()
    brepgprop_VolumeProperties(shape, props)
    name=file.split("/")[1].split(".stl")[0]
    print name+" "+str(int(abs(props.Mass()/BlockHeight)))+" square meters"
    with open('output/areas.txt', 'ab') as f:
        f.write("\n"+name+","+str(int(abs(props.Mass()/BlockHeight))))
    f.closed

def exportGeometry():
    with open('output/buildings.stl', 'w') as f:
        f.write(exporters.toString(buildings, 'STL'))
    f.closed
    with open('output/sidewalks.stl', 'w') as f:
        f.write(exporters.toString(sidewalks, 'STL'))
    f.closed
    with open('output/grass.stl', 'w') as f:
        f.write(exporters.toString(grass, 'STL'))
    f.closed
    with open('output/roads.stl', 'w') as f:
        f.write(exporters.toString(roads, 'STL'))
    f.closed
    
    print "gfa: "+str(int(totalGFA))+" square meters"
    with open('output/areas.txt', 'wb') as f:
        f.write("totalgfa,"+str(int(totalGFA)))
    f.closed
    
    getArea("output/grass.stl")
    getArea("output/roads.stl")
    getArea("output/sidewalks.stl")
    
exportGeometry()
