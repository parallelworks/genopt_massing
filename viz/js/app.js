var Header = React.createClass({displayName: "Header",
    render: function () {
        return (
            React.createElement("div", {className: "navbar navbar-default navbar-static-top", role: "navigation"}, 
                React.createElement("div", {className: "container"}, 
                    React.createElement("div", {className: "navbar-header"}, this.props.text)
                )
            )
        );
    }
});

var SearchBar = React.createClass({displayName: "SearchBar",
    searchKeyChangeHandler: function() {
        var searchKey = this.refs.searchKey.getDOMNode().value;
        this.props.searchKeyChange(searchKey);
    },
    clearText: function() {
        this.refs.searchKey.getDOMNode().value = "";
        this.props.searchKeyChange("");
    },
    render: function () {
        return (
            React.createElement("div", {className: "search-wrapper"}, 
                React.createElement("input", {type: "search", ref: "searchKey", className: "form-control", 
                    placeholder: "Enter a app, workflow or visualization search term", 
                    value: this.props.searchKey, 
                    onChange: this.searchKeyChangeHandler}), 
                React.createElement("button", {className: "btn btn-link"}, React.createElement("span", {className: "glyphicon glyphicon-remove", "aria-hidden": "true", onClick: this.clearText}))
            )
        );
    }
});

// Wrap nouislider (http://refreshless.com/nouislider) as a React component
var RangeSlider = React.createClass({displayName: "RangeSlider",
    componentDidMount: function() {
        var $el = $(this.refs.slider.getDOMNode()),
            changeHandler = this.props.onChange;
        $el.noUiSlider({
            start: [ this.props.min, this.props.max ],
            connect: true,
            step: this.props.step,
            range: {
                'min': this.props.min,
                'max': this.props.max
            }
        });
        //$el.noUiSlider_pips({
        //    mode: 'steps',
        //    filter: function(value) {
        //        return value == Math.floor(value);
        //    },
        //    density: 2
        //});
        $el.Link('lower').to('-inline-<div class="tooltip"></div>', function ( value ) {
            $(this).html(
                '<span>' + value.substr(0, value.length - 1) + '</span>'
            );
        });
        $el.Link('upper').to('-inline-<div class="tooltip"></div>', function ( value ) {
            $(this).html(
                '<span>' + value.substr(0, value.length - 1) + '</span>'
            );
        });
        $el.on({
            change: function(event){
                changeHandler($el.val());
            }
        });
    },
    render: function () {
        return (
            React.createElement("div", {className: "slider-wrapper"}, 
                React.createElement("span", {className: "slider-label"}, this.props.label), 
                React.createElement("div", {ref: "slider"})
            )
        );
    }
});

String.prototype.trunc = String.prototype.trunc ||
    function(n) {
        return this.length > n ? this.substr(0, n - 1) + '...' : this;
    };

var mode="shot";

var ResultListItem = React.createClass({displayName: "ResultListItem",
    linkHandler: function(e) {
        var model = e.target.id;
        var url='item/?model=' + model;
        window.open(url,'_blank');
        //this.props.searchKeyChange(e.target.innerHTML);
        //return false;
    },
    render: function () {
        var links;
        if (this.props.result.tags) {
            var tags = this.props.result.tags.split(', ');
            links = tags.map(function(tag) {
                return React.createElement("a", {href: "#", className: "tag", onClick: this.linkHandler, key: this.props.result.id + '-' + tag}, tag);
            }.bind(this));
        }
        
        if (mode=="shot"){
            var image = this.props.result.shot
        }else if (mode=="plot"){
            var image = this.props.result.plot
        }else if (mode=="summary"){
            var image = this.props.result.summary
        }
        
        var name = this.props.result.brewery.trunc(40, true);
        
        return (
            React.createElement("div", {className: "col-lg-3 col-md-4 col-sm-6 col-xs-12 nopadding", key: this.props.result.id}, 
                React.createElement("div", {className: "panel panel-default",onClick:this.linkHandler}, 
                React.createElement("div", {className: "overlay","id":this.props.result.brewery}), 
                    React.createElement("div", {className: "panel-body"}, 
                        React.createElement("img", {src: image,style:{"width": "250px","height": "250px"}, className: "img-wrapper"}), 
                        React.createElement("div",{style:{"font-size":"10px","margin-top":"6px"}},name))
                )
                
            )
        );
    }
    // React.createElement("h4", {className: "panel-title"}, this.props.result.name), 
    //React.createElement("p", null, React.createElement("img", {src: "pics/icon-tags.png", className: "icon"}), links)
});

var ResultList = React.createClass({displayName: "ResultList",
    render: function () {
        var items = this.props.results.map(function (result) {
            return (
                React.createElement(ResultListItem, {key: result.id, result: result, searchKeyChange: this.props.searchKeyChange})
            );
        }.bind(this));
        return (
            React.createElement("div", {className: "container"}, 
                React.createElement("div", {className: "row"}, 
                    items
                )
            )
        );
    }
});

var Paginator = React.createClass({displayName: "Paginator",

     changeHandler: function(e) {
        mode = e.target.value;
        this.setState({selected : e.target.value });
        React.render(React.createElement(App, null), document.getElementById('main'));
      },
     changeHandlerView: function(e) {
        previewMax = e.target.value;
        this.setState({selected : e.target.value });
        this.props.findresults();
        //React.findResults(React.createElement(App, null), document.getElementById('main'));
      },
  
    render: function () {
        var pages = Math.ceil(this.props.total/this.props.pageSize);
        
        return (
            React.createElement("div", {className: "container"}, 
                React.createElement("div", {className: "row padding", style: {height: "40px"}}, 
                    React.createElement("div", {className: "col-xs-4 nopadding"}, 
                        React.createElement("button", {type: "button", className: "btn btn-default" + (this.props.page <= 1 ? " hidden" : ""), onClick: this.props.previous}, 
                            React.createElement("span", {className: "glyphicon glyphicon-chevron-left", "aria-hidden": "true"}), " Previous"
                        )
                    ), 
                    React.createElement("div", {className: "col-xs-4 text-center",style:{"font-size":"12px"}}, 
                        React.createElement("div", {className: "legend"}, this.props.total, " results • view ",
                            React.createElement("select", { onChange: this.changeHandlerView,value: previewMax },
                                 React.createElement("option", { value: 8 }, "8"),
                                 React.createElement("option", { value: 16 }, "16"),
                                 React.createElement("option", { value: 32 }, "32"),
                                 React.createElement("option", { value: 64 }, "64")
                            )," • preview ",
                            React.createElement("select", { onChange: this.changeHandler,value: mode },
                                 React.createElement("option", { value: "shot" }, "isometric"),
                                 React.createElement("option", { value: "plot" }, "plot"),
                                 React.createElement("option", { value: "summary" }, "radar")
                            )," • page ", this.props.page, "/", pages
                        )
                    ), 
                    React.createElement("div", {className: "col-xs-4 nopadding"}, 
                        React.createElement("button", {type: "button", className: "btn btn-default pull-right" + (this.props.page >= pages ? " hidden" : ""), onClick: this.props.next}, 
                        "Next ", React.createElement("span", {className: "glyphicon glyphicon-chevron-right", "aria-hidden": "true"})
                        )
                    )
                )
            )
        );
    }
});

var previewMax = 16;

var App = React.createClass({displayName: "App",
    getInitialState: function() {
        return {
            searchKey: "",
            min: 0,
            max: previewMax,
            results: [],
            total: 0,
            page: 1
        }
    },
    componentDidMount: function() {
        this.findResults();
    },
    searchKeyChangeHandler: function(searchKey) {
        this.setState({searchKey: searchKey, page: 1}, this.findResults);
    },
    rangeChangeHandler: function(values) {
        this.setState({min: values[0], max: values[1], page: 1}, this.findResults);
    },
    findResults: function() {
        var data = resultService.findAll({search: this.state.searchKey, min: this.state.min, max: previewMax, page: this.state.page});
          this.setState({
                results: data.results,
                page: data.page,
                pageSize: data.pageSize,
                total: data.total
            });
    },
    nextPage: function() {
        var p = this.state.page + 1;
        this.setState({page: p}, this.findResults);
    },
    prevPage: function() {
        var p = this.state.page - 1;
        this.setState({page: p}, this.findResults);
    },
    render: function() {
        return (
            React.createElement("div", null, 
                React.createElement(Header, {text: ""}), 
                React.createElement("div", {className: "container"}, 
                    React.createElement("div", {className: "row"}
                    )
                ), 
                React.createElement(Paginator, {page: this.state.page, pageSize: this.state.pageSize, total: this.state.total, previous: this.prevPage, next: this.nextPage,findresults:this.findResults}), 
                React.createElement(ResultList, {results: this.state.results, total: this.state.total, searchKeyChange: this.searchKeyChangeHandler})
            )
        );
    }
});

loadData();

setTimeout(function(){
    React.render(React.createElement(App, null), document.getElementById('main'));
},1000)
