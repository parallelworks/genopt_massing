var results = [];

var resultFile = window.location.search.split("run=")[1];

var loadData = function() {

    // read the result text file

    //$.get('http://s3.parallel.works/liggghts/html/'+resultFile+'.txt', function(data) {
    $.get('../run/results/results.txt', function(data) {
        var data = data.split("\n");

        for (var i = data.length-1; i >= 0; i--) {

            if (data[i] != "") {
                var model = data[i];
                    
                console.log(model);
                    
                results.push({
                    tags: 'Apps, AEC, CFD, Mechanical, Optimization, Parametrics',
                    id: i,
                    name: model,
                    alcohol: 'Free',
                    brewery: model,
                    summary: '../run/results/polar_plots/' + model,
                    plot: '../run/results/plots/' + model,
                    shot: '../run/results/renders_overview_pad/' + model
                })
            }
        }
    })
}

resultService = (function() {

    var baseURL = "";

    // The public API
    return {
        findById: function(id) {
            //return $.ajax(baseURL + "/results/" + id);
            return null
        },
        findAll: function(values) {

            // get the items
            var pageSize = values.max;
            var page = values.page;
            var total = results.length;

            var resultsPush = [];

            for (var i = (page - 1) * pageSize; i < pageSize * page; i++) {
                if (results[i] != undefined) {
                    resultsPush.push(results[i]);
                }
            }

            var json = {
                "pageSize": pageSize,
                "page": page,
                "total": total,
                "results": resultsPush
            };


            return json

        }
    };

}());